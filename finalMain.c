#include <stdlib.h>
#include <stdio.h>
#include "include/SDL/SDL.h"
#include "include/sfd/sfd.h"
#include <string.h>
#include <tchar.h>

int checkKey(const char * pathOfFile){                         //Fonction vérifiaction du fichier clé
  char indexOfIdentity[5] = "9999\0", stringOfKey[33], currentChar;   //9 est le code erreur pour la suite
  int i = 0;
  FILE *key = fopen(pathOfFile, "rb");                                //Ouverture en mode lecture du fichier clé de type binaire
  if(key == NULL) return 0;                                           //Si l'ouverture du fichier a échoué on retourne le code erreur
  //currentChar = '\0';
  fseek (key , 5 , SEEK_SET );                                        //placement du curseur sur le début de la chaine binaire
  while(currentChar != ']' || !feof(key)){                            //récupération de la clé dans le tableau stringOfKey
    currentChar = fgetc(key);
    if(currentChar == ']') break;
    if(currentChar == ' ')continue;
    if(currentChar != '1' && currentChar != '0') return 0;
    stringOfKey[i] = currentChar;
    i++;
  }
  stringOfKey[i] = '\0';                                             //placement du caractère de fin de chaine

  if(i != 32) return 0;                                               //si la clé ne fait pas 32 bits retourner le code erreur

  fseek(key, 0, SEEK_END);                                            //si le fichier fait plus de 43 octets retour du code erreur
  i = ftell(key);
  if(i != 43) return 0;
  fclose(key);


  for(i = 0; i < 8; i++){                                               // récupération de la matrice d'identité
  if(stringOfKey[i] == '1' && stringOfKey[i + 8] == '0' && stringOfKey[i + 8 * 2] == '0' && stringOfKey[i + 8 * 3] == '0'){
    indexOfIdentity[0] = i;
  }
  else if(stringOfKey[i] == '0' && stringOfKey[i + 8] == '1' && stringOfKey[i + 8 * 2] == '0' && stringOfKey[i + 8 * 3] == '0'){
    indexOfIdentity[1] = i;
  }
  else if(stringOfKey[i] == '0' && stringOfKey[i + 8] == '0' && stringOfKey[i + 8 * 2] == '1' && stringOfKey[i + 8 * 3] == '0'){
    indexOfIdentity[2] = i;
  }
  else if(stringOfKey[i] == '0' && stringOfKey[i + 8] == '0' && stringOfKey[i + 8 * 2] == '0' && stringOfKey[i + 8 * 3] == '1'){
    indexOfIdentity[3] = i;
  }
}
if(strchr(indexOfIdentity, '9')) return 0;                              //si la fonction trouve un 9, la matrice d'identité est imcomplète

return 1;                                                              //Si aucune erreur n'a été trouvée on renvoie le code success
}


int codage(char * fileName, char * keyName){                             //Fonction de codage
  unsigned char* pathOfFile = fileName;
  unsigned char* pathOfKey = keyName;
  unsigned char* pathOfFinalFile;
  unsigned char stringOfKey[33] = "\0", stringEncoded[17] = "\0";
  unsigned char hexaTemp[3] = "00\0", tempChar, binaryTemp[9] = "00000000\0";
  FILE *userFile = NULL, *userKey = NULL, *finalFile = NULL;
  int i = 0, y, tempHexa;
  int currentChar, count = 0, finalChar;

  pathOfFinalFile = malloc(sizeof(char) * strlen(pathOfFile) + 2);                //ajout de 'c' à l'extension  du fichier
  strcpy(pathOfFinalFile, pathOfFile);
  pathOfFinalFile[strlen(pathOfFile)] = 'c';
  pathOfFinalFile[strlen(pathOfFile) + 1] = '\0';
  userFile = fopen(pathOfFile, "rb");                                             //Ouverture du fichier à encoder en mode lecture
  userKey = fopen(pathOfKey, "rb");                                               //ouverture du fichier clé en mode lecture
  finalFile = fopen(pathOfFinalFile, "wb");                                       //ouverure du fichier destination en mode écriture
  if(userFile == NULL|| userKey == NULL || finalFile == NULL) return 0;          //vérification de l'ouverture des fichiers

  currentChar = '\0';
  fseek (userKey , 5 , SEEK_SET );                                                //récupération de la clé dans un tableau
  while(currentChar != ']' || !feof(userKey)){
    currentChar = fgetc(userKey);
    if(currentChar == ']') break;
    if(currentChar == ' ')continue;
    stringOfKey[i] = currentChar;
    i++;
  }
  stringOfKey[i] = '\0';
  fclose(userKey);
  /******************************************** lecture et conversion en binaire du fichier ********************************************/
  currentChar = '\0';
  while(1){                                               //traitement conversion en binaire, passage dans la matrice, conversion en ASCII puis écriture dans le fichier

    currentChar = fgetc(userFile);
    if(feof(userFile)) break;
    tempChar = currentChar;
    for(i = 1; i > -1; i--){                              //conversion ASCII => HEXA
      hexaTemp[i] = (int)tempChar % 16;
      tempChar = (int)tempChar / 16;
      tempHexa = hexaTemp[i];
      for(y = 7; y > -1; y--){                          //conversion HEXA => binaire
        if(i == 1 && y >=4){
          binaryTemp[y] = (tempHexa % 2) + 48;
          tempHexa /= 2;

        }
        else if(i == 0 && y < 4){
          binaryTemp[y] = (tempHexa % 2) + 48;
          tempHexa = tempHexa / 2;
        }
      }
    }
    /******************************************** encodage avec la matrice G4 ********************************************/
    count = 0;                                         //Passage des 8 bits dans la matrice
    for(i = 0; i < strlen(binaryTemp); i += 4){
      for(y = 0; y < 8; y++){
        if(((binaryTemp[i] - '0' && stringOfKey[y] - '0') + (binaryTemp[i + 1] - '0' && stringOfKey[y + 8] - '0') + (binaryTemp[i + 2] - '0' && stringOfKey[y + 8 * 2] - '0') + (binaryTemp[i + 3] - '0' && stringOfKey[y + 8 * 3] - '0')) % 2){
          stringEncoded[count] = '1';

        }
        else stringEncoded[count] = '0';
        count++;
      }
    }
    stringEncoded[count] = '\0';

    /******************************************** conversion du binaire encodé en ASCII ********************************************/
    i = 0;
    y = 128;
    finalChar = 0;                                //conversion binaire => ASCII
    while(stringEncoded[i] != '\0'){
      if(i % 8 == 0 && i != 0){
        y = 128;
        fputc(finalChar, finalFile);
        finalChar = 0;
      }
      finalChar += (stringEncoded[i] - 48) * y;
      y /= 2;
      i++;
    }
    fputc(finalChar, finalFile);                   //Ecriture du caractère dans le fichier
    free(pathOfFinalFile);
  }
  fclose(finalFile);
  fclose(userFile);
  return 1;
}


int decodage(char * fileName, char * keyName){        //fonction décodage
  unsigned char* pathOfKey = keyName;
  unsigned char* pathOfFile = fileName;
  int i, y, tempHexa, z;
  char indexOfIdentity[5], secondChar;
  char * finalBinary = NULL;
  char * finalPath = NULL;
  unsigned char* stringUserBinary;
  unsigned char stringOfKey[33] = "\0";
  unsigned char * stringX;
  unsigned char hexaTemp[3] = "00\0", tempChar, binaryTemp[17] = "0000000000000000\0", userChoice[2];
  FILE* userFile = NULL;
  FILE* userKey = NULL;
  FILE* finalFile = NULL;
  int next = 0;
  int sizeOfFile = 0, currentChar, count = 0, finalChar;

  finalPath = malloc(sizeof(char) * strlen(pathOfFile) + 2);        //ajout de 'd' à l'extension du fichier
  strcpy(finalPath, pathOfFile);
  finalPath[strlen(pathOfFile)] = 'd';
  finalPath[strlen(pathOfFile) + 1] = '\0';


  userFile = fopen(pathOfFile, "rb");                              //ouverture fichiers
  userKey = fopen(pathOfKey, "rb");
  finalFile = fopen(finalPath, "wb");
  if(userFile == NULL || userKey == NULL || finalFile == NULL) return 0;   //code erreur si l'ouverture d'un fchier a raté
  fseek (userKey , 5 , SEEK_SET );                                         //copie de la clé dans un tableau
  currentChar = '\0';
  i = 0;
  while(currentChar != ']' || !feof(userKey)){
    currentChar = fgetc(userKey);
    if(currentChar == ']') break;
    if(currentChar == ' ')continue;
    stringOfKey[i] = currentChar;
    i++;

  }
  stringOfKey[i] = '\0';
  fclose(userKey);

  for(i = 0; i < 8; i++){                                                  //obtention de la matrice d'identité dans un tableau
    if(stringOfKey[i] == '1' && stringOfKey[i + 8] == '0' && stringOfKey[i + 8 * 2] == '0' && stringOfKey[i + 8 * 3] == '0'){
      indexOfIdentity[0] = i;
    }
    else if(stringOfKey[i] == '0' && stringOfKey[i + 8] == '1' && stringOfKey[i + 8 * 2] == '0' && stringOfKey[i + 8 * 3] == '0'){
      indexOfIdentity[1] = i;
    }
    else if(stringOfKey[i] == '0' && stringOfKey[i + 8] == '0' && stringOfKey[i + 8 * 2] == '1' && stringOfKey[i + 8 * 3] == '0'){
      indexOfIdentity[2] = i;
    }
    else if(stringOfKey[i] == '0' && stringOfKey[i + 8] == '0' && stringOfKey[i + 8 * 2] == '0' && stringOfKey[i + 8 * 3] == '1'){
      indexOfIdentity[3] = i;
    }
  }

  currentChar = '\0';
  while(1){
    for(i = 0; i < 4; i++){                     //conversion de deux carctères par deux
      if(i % 2 == 0){                          //récupérer le carctère suivant
        currentChar = fgetc(userFile);
        if(feof(userFile)){                   //fin du traitement
          fclose(finalFile);
          fclose(userFile);
          free(finalPath);
          return 1;
        }
      }
      tempHexa = (int)currentChar % 16;             //conversion ASCII => HEXA
      currentChar/= 16;
      if(i < 2) next = 7;
      else next = 15;
      for(y = next -(i%2) * 4; y > next -((i%2) + 1) * 4; y--){ //conversion HEXA => binaire
        binaryTemp[y] = (tempHexa % 2) + 48;
        tempHexa /= 2;
      }
    }
    i = 0;
    y = 128;
    finalChar = 0;                                            //conversion binaire => ASCII
    for(i = 0; i < strlen(binaryTemp); i+= 8){
      for(z = 0; z < 4; z++){
        finalChar += (binaryTemp[indexOfIdentity[z] + i] - 48) * y;
        y /= 2;
      }
    }
    fputc(finalChar, finalFile);                               //insertion du cacractère
  }
}

void ligneHorizontale(int x, int y, int w, Uint32 coul, SDL_Surface *ecran)
{
  SDL_Rect r;
  r.x = x;
  r.y = y;
  r.w = w;
  r.h = 5;

  SDL_FillRect(ecran, &r, coul);
}

void ligneVerticale(int x, int y, int h, Uint32 coul, SDL_Surface *ecran)
{
  SDL_Rect r;
  r.x = x;
  r.y = y;
  r.w = 3;
  r.h = h;
  SDL_FillRect(ecran, &r, coul);
}

void rectangle(int x, int y, int w, int h, Uint32 coul, SDL_Surface *ecran)
{
  ligneHorizontale(x, y, w, coul, ecran);
  ligneHorizontale(x, y + h - 1, w, coul, ecran);
  ligneVerticale(x, y + 1, h - 2, coul, ecran);
  ligneVerticale(x + w - 1, y + 1, h - 2, coul, ecran);
}

int main(int argc, char *argv[])
{
  SDL_Surface *ecran = NULL, *imageDeFond = NULL, *affichage = NULL;
  SDL_Rect positionFond;
  SDL_Event event;
  int check = 1;
  Uint32 coul, coul1;
  int checkUpFile[2] = {0};
  char * pathOfKey = NULL, *pathOfFile = NULL;
  positionFond.x = 0;
  positionFond.y = 0;

  SDL_Init(SDL_INIT_VIDEO);
  ecran = SDL_SetVideoMode(600, 400, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
  SDL_WM_SetCaption("CODECG4", NULL);

  /* Chargement d'une image Bitmap dans une surface */
  imageDeFond = SDL_LoadBMP("image/CODECG4.bmp");
  /* On blitte par-dessus l'écran */
  SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
  SDL_Flip(ecran);
  SDL_FreeSurface(imageDeFond); /* On libère la surface */
  while(check){

    SDL_WaitEvent(&event);
    switch(event.type){
      case SDL_QUIT:
      check = 0;
      break;
      case SDL_MOUSEBUTTONUP:
      if(event.button.button == SDL_BUTTON_LEFT){
        if(event.button.x > 57 && event.button.x < 397 && event.button.y > 43 && event.button.y < 157){
          sfd_Options opt = {
            .title        = "Open Image File",
          };

          const char *fileName = sfd_open_dialog(&opt);

          if (fileName) {
            checkUpFile[0] = 1;
            pathOfFile = malloc(sizeof(char) * strlen(fileName) + 1);
            strcpy(pathOfFile, fileName);
            coul = SDL_MapRGB(ecran->format, 0, 255, 0);
            rectangle(54, 40, 343, 117, coul, ecran);
            SDL_Flip(ecran);
          } else {
            checkUpFile[0] = 0;
            coul = SDL_MapRGB(ecran->format, 255, 0, 0);
            rectangle(54, 40, 343, 117, coul, ecran);
            SDL_Flip(ecran);
          }
        }
        else if(event.button.x > 57 && event.button.x < 397 && event.button.y > 212 && event.button.y < 326){
          sfd_Options opt = {
            .title        = "Open Image File",
          };

          const char *keyName = sfd_open_dialog(&opt);


          if (keyName && checkKey(keyName)) {
            checkUpFile[1] = 1;
            pathOfKey = malloc(sizeof(char) * strlen(keyName) + 1);
            strcpy(pathOfKey, keyName);
            coul = SDL_MapRGB(ecran->format, 0, 255, 0);
            rectangle(54, 209, 343, 117, coul, ecran);
            SDL_Flip(ecran);
          } else {
            checkUpFile[1] = 0;
            coul = SDL_MapRGB(ecran->format, 255, 0, 0);
            rectangle(54, 209, 343, 117, coul, ecran);
            SDL_Flip(ecran);
          }
        }
        else if(event.button.x > 454 && event.button.x < 563 && event.button.y > 43 && event.button.y < 155 && (checkUpFile[0] + checkUpFile[1] == 2)){
          if(codage(pathOfFile, pathOfKey)){
            free(pathOfKey);
            free(pathOfFile);
            checkUpFile[0] = 0;
            checkUpFile[1] = 0;
            coul = SDL_MapRGB(ecran->format, 255, 255, 255);
            rectangle(54, 40, 343, 117, coul, ecran);
            rectangle(54, 209, 343, 117, coul, ecran);
            SDL_Flip(ecran);
          }
        }
        else if(event.button.x > 453 && event.button.x < 566 && event.button.y > 212 && event.button.y < 324 && (checkUpFile[0] + checkUpFile[1] == 2)){
          if(decodage(pathOfFile, pathOfKey)){
            free(pathOfKey);
            free(pathOfFile);
            checkUpFile[0] = 0;
            checkUpFile[1] = 0;
            coul = SDL_MapRGB(ecran->format, 255, 255, 255);
            rectangle(54, 40, 343, 117, coul, ecran);
            rectangle(54, 209, 343, 117, coul, ecran);
            SDL_Flip(ecran);
          }
        }

      }
      break;
    }
  }
  SDL_Quit();

  return EXIT_SUCCESS;
}
